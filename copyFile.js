import  fs from "fs"
import fsp from "fs/promises"
import path from "path"

function copyFileSync(file){
  try {
    log("start")
    const dir = fs.mkdtempSync("sync-")
    const buffer = fs.readFileSync(file)
    fs.writeFileSync(path.join(dir, file), buffer)
    log(`${file} written to ${dir}`)
  } catch(r){
    log("catch " + r)
  }
}

// dit werkt niet: een asynchrone functie eindigt onmiddellijk
// je moet verder werken in de callback,
// want enkel daar vind je  het resultaat
function copyFileTest(file){
  try {
    log("start")
    const dir = fs.mkdtempSync("test-")
    fs.readFile(file,(err ,buffer)=> {
      if (err)  { log ("leesfout: " + err)}
    })
    fs.writeFile(path.join(dir, file), buffer,err => {
      if (err)  { log ("schrijffout: " + err)}
    } )
    log(`${file} written to ${dir}`)
  } catch(r){
    log("catch " + r)
  }
}

export function copyFileAsync(file){
  try {
    log("start")
     fs.mkdtemp("async-",(err,dir) =>
       fs.readFile(file,(err ,buffer)=> {
         if (err)  { log ("leesfout: " + err)}
         else fs.writeFile(path.join(dir, file), buffer,err => {
           if (err)  { log ("schrijffout: " + err)}
           log(`${file} written to ${dir}`)
         } )
       })
     )
    log("Dit volgt onmiddellijk")
  } catch(r){
    log("catch " + r)
  }
}

function copyFilePromiseProblem(file){
  try {
    log("start")
    fsp.mkdtemp("promise-")
      .then(dir =>  {
        return fsp.readFile(file)
      })
      .then(buffer =>
      // this went wrong in the demo,
      // we want to use dir but it is not known here.
      // That is because this call is not nested anymore in mkdtemp callback
      // so dir is only known in the previous then
      // this then only gets the buffer returned by readFile
        fsp.writeFile(path.join(tmpdir, file),buffer))
      .then(result => log(`${file} written to ${tmpdir}`))
      .catch(err => log ("fout bij copy file:" + err));
    log("Dit volgt onmiddellijk")
  } catch(r){
    log("catch " + r)
  }
}

function copyFilePromise(file){
  try {
    let tmpdir
    log("start")
    fsp.mkdtemp("promise-")
      .then(dir =>  {
        //  solution 1 for dir is not known
        //  assign dir to a vaiable that is in a higher scope
        //  tmpdir is known in all thens
        tmpdir=dir
        return fsp.readFile(file)
      })
      .then(buffer => fsp.writeFile(path.join(tmpdir, file),buffer))
      .then(result => log(`${file} written to ${tmpdir}`))
      .catch(err => log ("fout bij copy file:" + err));
    log("Dit volgt onmiddellijk")
  } catch(r){
    log("catch " + r)
  }
}

function copyFilePromise2(file){
  try {
    let tmpdir
    log("start")
    fsp.mkdtemp("promise-")
      .then(dir =>
        // solution 2 for dir not known:
        // return both buffer and dir to the next then
        // by wrapping them in a new object
         Promise.all([Promise.resolve(dir),fsp.readFile(file)])
  )
      .then(o => fsp.writeFile(path.join(o[0], file),o[1]))
      .then(result => log(`${file} written to ${tmpdir}`))
      .catch(err => log ("fout bij copy file:" + err));
    log("Dit volgt onmiddellijk")
  } catch(r){
    log("catch " + r)
  }
}

function copyFilePromise2(file){
  try {
    let tmpdir
    log("start")
    fsp.mkdtemp("promise-")
      .then(dir =>
        // solution 2 for dir not known:
        // we return an array of promises with all information we want in the next then
        // dir must be wrapped in a promise (that is already resolved)
        Promise.all([Promise.resolve(dir),fsp.readFile(file)])
      )
      .then(o => fsp.writeFile(path.join(o[0], file),o[1]))
      // .then(result => log(`${file} written to ${tmpdir}`))
      // if you want tempdir to be known here as well
      // you have to repeat the  Promise.all trick in the previous then as well
      .then(result => log(`${file} written` ))
  
      .catch(err => log ("fout bij copy file:" + err));
    log("Dit volgt onmiddellijk")
  } catch(r){
    log("catch " + r)
  }
}

async function copyFileAwait(file){
  try {
    log("start")
    const dir = await fsp.mkdtemp("await-")
    const buffer = await  fsp.readFile(file)
    await fsp.writeFile(path.join(dir, file),buffer)
    log(`${file} written to ${dir}`)
  } catch(r){
    log("catch " + r)
  }
}

function log(txt){
  console.log(`${timestamp()}  ${txt}`)
}

function timestamp() {
  const now = new Date()
  return `${now.getMinutes()}:${now.getSeconds()}:${now.getMilliseconds()}`
}

//copyFileSync("sample.txt")
//copyFileTest("sample.txt")
//copyFileAsync("sample.txt")
//copyFilePromiseProblem("sample.txt")
//copyFilePromise("sample.txt")
copyFilePromise2("sample.txt")
//copyFileAwait("sample.txt")

